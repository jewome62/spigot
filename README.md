# Spigot
A Docker image for running Spigot.

[![build status](https://gitlab.com/docker-minecraft/spigot/badges/master/build.svg)](https://gitlab.com/docker-minecraft/spigot/commits/master)

# Installation
Here are the available versions of building Spigot:  https://gitlab.com/jewome62/spigot/container_registry

Let's say we're running Minecraft version 1.13.1, simply clone and build the image:

```
$ docker run -p 25577:25577 -e "JAVA_ARGS=-Xmx1G" -v $(pwd)/my-spigot:/data -itd --name my-spigot jewome62/spigot:1.13.1
$ docker attach my-spigot
```