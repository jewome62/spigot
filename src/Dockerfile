FROM openjdk:8-jre-alpine

ARG SPIGOT_BUILDTOOLS_URL=https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
ARG SPIGOT_VERSION=latest
ENV JAVA_ARGS ""
ENV SPIGOT_ARGS ""

WORKDIR /data
VOLUME "/data"

ADD "${SPIGOT_BUILDTOOLS_URL}" /tmp/spigot/BuildTools.jar

ENV GOSU_VERSION 1.10
RUN set -ex; \
    apk add --no-cache gosu; \
	apk add --no-cache --virtual .build-deps \
		wget \
		git \
	; \
	gosu nobody true; \
    cd /tmp/spigot; \
    java -jar BuildTools.jar --rev ${SPIGOT_VERSION}; \
    cp spigot-*.jar /srv/spigot.jar; \
    rm -rf /tmp/spigot; \
    chmod 0444 /srv/spigot.jar; \
    apk del .build-deps; \
    rm -rf /var/lib/{cache,log}/ /tmp/* /var/tmp/*

COPY run_spigot.sh /usr/local/bin/spigot
RUN chmod +x /usr/local/bin/spigot

ENTRYPOINT ["spigot"]