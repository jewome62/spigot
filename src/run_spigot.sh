#!/bin/bash

USER_ID=${LOCAL_USER_ID:-9001}
id -u user &>/dev/null || useradd --shell /bin/bash -u $USER_ID -o -c "" -m user
export HOME=/home/user
chown -R user:user /srv
chown -R user:user /data
# exec gosu user "$@"
exec gosu user java $JAVA_ARGS -jar /srv/spigot.jar $SPIGOT_ARGS $@